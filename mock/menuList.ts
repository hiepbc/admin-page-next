import { MenuModel } from "../interfaces/menu";

export const FakeMenu: MenuModel[] = [
  {
    id: '314bde1c-c479-11e9-aa8c-2a2ae2dbcce4',
    name: "Windows",
    iconType: "windows",
    children: [
      {
        id: '314be09c-c479-11e9-aa8c-2a2ae2dbcce4',
        name: "IE",
        iconType: 'ie',
        children: [
          {
            id: '314be1f0-c479-11e9-aa8c-2a2ae2dbcce4',
            name: "github",
            iconType: 'github'
          },
          {
            id: '314be326-c479-11e9-aa8c-2a2ae2dbcce4',
            name: "Weibo",
            iconType: 'weibo-square',
            children: [
              {
                id: '314be45c-c479-11e9-aa8c-2a2ae2dbcce4',
                name: 'Weibo Circle',
              },
              {
                id: '314be718-c479-11e9-aa8c-2a2ae2dbcce4',
                name: 'Taobao Circle',
              }
            ]
          }
        ]
      },
      {
        id: '314be86c-c479-11e9-aa8c-2a2ae2dbcce4',
        name: "Instagram",
        iconType: 'instagram'
      }
    ]
  },
  {
    id: '314be998-c479-11e9-aa8c-2a2ae2dbcce4',
    name: "Google",
    iconType: "google",
    children: [
      {
        id: '314beac4-c479-11e9-aa8c-2a2ae2dbcce4',
        name: "Google plus",
        iconType: 'google-plus'
      },
      {
        id: '314bebe6-c479-11e9-aa8c-2a2ae2dbcce4',
        name: "Facebook",
        iconType: 'facebook'
      },
      {
        id: '314bed12-c479-11e9-aa8c-2a2ae2dbcce4',
        name: "Ant design"
      }
    ]
  },
  {
    id: '314bf00a-c479-11e9-aa8c-2a2ae2dbcce4',
    name: "Tables",
    iconType: "table",
    children: [
      {
        id: '314bf140-c479-11e9-aa8c-2a2ae2dbcce4',
        name: "Table"
      },
      {
        id: '314bf262-c479-11e9-aa8c-2a2ae2dbcce4',
        name: "Data Table",
        children: [
          {
            id: '314bf38e-c479-11e9-aa8c-2a2ae2dbcce4',
            name: 'Many table',
          },
          {
            id: '2c5fbb46-309f-4e20-8439-18e9cfa2eda0',
            name: 'Col table',
          }
        ]
      }
    ]
  },
  {
    id: '29b60525-60fe-4a80-aad9-6e511d6ff3b4',
    name: "Twitter",
    iconType: "twitter",
  }
];
