import { CurrentUser } from "../interfaces/user";

export const FakeUser: CurrentUser = {
  name: "Hiep BC",
  avatar:
    "https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png"
};
