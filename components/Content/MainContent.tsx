import React, { ReactNode } from 'react'
import { Layout } from 'antd';
const { Content } = Layout;

interface Props {
  children: ReactNode
}

const MainContent = (props: Props) => {
  return (
    <Content style={{ padding: '20px 20px' }}>
      <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
        {props.children}
      </div>
    </Content>
  )
}

MainContent.defaultProps = {
  children: 'Content'
}

export default MainContent
