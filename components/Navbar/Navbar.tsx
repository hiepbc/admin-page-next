import { Layout } from 'antd'

import RightContent from './RightContent'
const { Header } = Layout
import { FakeUser } from '../../mock/user'
import LeftContent from './LeftContent'

interface Props {
  toggle: () => void,
  collapsed: boolean,
  children?: JSX.Element
}

const Navbar = (props: Props) => {
  return (
    <>
      <Header className="navbar-header">
        <LeftContent collapsed={props.collapsed} toggle={props.toggle} />
        {props.children}
        <RightContent currentUser={FakeUser} />
      </Header>
      <style>{`
        @media (max-width: 576px) {
          .navbar-header {
            display: flex;
            flex-direction: column;
          }
        }
      `}</style>
    </>
  )
}

export default Navbar
