import { Avatar, Spin } from 'antd'

import { CurrentUser } from '../../interfaces/user'

export interface Props {
  currentUser?: CurrentUser;
  menu?: boolean
}

const AvatarContent = (props: Props) => {
  return props.currentUser && props.currentUser.name ? (
    <span>
      <Avatar size="small" alt="avatar"
        src={props.currentUser.avatar}
        className="avatar"
      />
      <span>{props.currentUser.name}</span>
    </span>
  ) : (
      <Spin size="small" style={{ marginLeft: 8, marginRight: 8 }} />
    )
}

export default AvatarContent
