import { Icon } from 'antd'

import Logo from './Logo'

interface Props {
  toggle: () => void,
  collapsed: boolean,
}

const LeftContent = (props: Props) => {
  return (
    <div className="left-content">
      <Logo imageUrl="static/images/api_icon.png" title="Ant Design Admin" />
      <Icon
        className="trigger"
        type={props.collapsed ? 'menu-unfold' : 'menu-fold'}
        onClick={props.toggle}
      />
      <style>{`
        .left-content {
          height: 68px;
          display: flex;
          line-height: 68px;
        }

        @media (max-width: 576px) {
          .trigger {
            display: none; 
          }
        }
      `}</style>
    </div>
  )
}

export default LeftContent
