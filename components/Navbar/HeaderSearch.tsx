import { useState } from 'react'
import { AutoComplete, Icon, Input } from 'antd'
import classNames from 'classnames'

const HeaderSearch = () => {
  const [showSearch, setShowSearch] = useState<boolean>(false)
  const [inputRef, setInputRef] = useState<null | Input>(null)
  // let inputRef: Input | null = null

  const enterSearchMode = () => {
    setShowSearch(prevState => !prevState)

    if (inputRef) {
      inputRef.focus()
    }
  }

  const leaveSearch = () => {
    setShowSearch(false)
  }

  return (
    <span className="header-search">
      <Icon type="search" key="Icon" onClick={enterSearchMode} />
      <AutoComplete key="AutoComplete"
        className={classNames('input', { 'show-input': showSearch })}
      >
        <Input
          ref={node => {
            setInputRef(node)
          }}
          onBlur={leaveSearch}
        />
      </AutoComplete>
      <style>{`
        @media (max-width: 576px) {
          .header-search .show-input {
            width: 100px;
          }
        }
      `}</style>
    </span>
  )
}

export default HeaderSearch
