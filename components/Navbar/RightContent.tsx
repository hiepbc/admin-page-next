import HeaderSearch from './HeaderSearch'
import AvatarContent from './AvatarContent'
import { CurrentUser } from '../../interfaces/user'

interface Props {
  currentUser?: CurrentUser
}

const RightContent = (props: Props) => {
  return (
    <div className="right-content">
      <div>
        <HeaderSearch />
      </div>
      <AvatarContent currentUser={props.currentUser} />
      <style>{`
        .right-content {
          display: flex;
        }

        @media (max-width: 576px) {
          .right-content {
            
          }
        }
      `}</style>
    </div>
  )
}

export default RightContent