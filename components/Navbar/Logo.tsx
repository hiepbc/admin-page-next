import { Avatar, Typography } from 'antd'

const { Title } = Typography;

interface Props {
  title: string,
  imageUrl: string
}

const Logo = (props: Props) => {
  return (
    <div className="logo">
      <Title level={4}>{props.title}</Title>
      <Avatar size={50} src={props.imageUrl} icon='ant-design' />
      <style>{`
        .logo {
          width: 254px;
          display: flex;
          align-items: center;
          justify-content: space-around;
        }
      `}</style>
    </div>
  )
}

export default Logo
