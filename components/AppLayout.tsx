import React, { useState } from 'react'
import Head from 'next/head'
import { Layout } from 'antd'
import classNames from 'classnames'

import MainSider from './Sider/MainSider'
import Navbar from './Navbar/Navbar'
import MainContent from './Content/MainContent'
import MainFooter from './Footer/MainFooter'
import { FakeMenu } from '../mock/menuList'

type Props = {
  title?: string
}

const AppLayout: React.FunctionComponent<Props> = ({
  // children,
  title = 'This is the default title',
}) => {
  const [collapsed, setCollapsed] = useState(false)
  const toggle = () => {
    setCollapsed(prevCollapsed => !prevCollapsed)
  };

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Layout className='app-layout'>
        <header>
          <Navbar collapsed={collapsed} toggle={toggle}></Navbar>
        </header>
        <div className="body">
          <div className={classNames("main-sider", { 'main-sider-show': collapsed })}>
            <MainSider collapsed={collapsed} listSidebar={FakeMenu} />
          </div>
          <Layout>
            <MainContent><h1>Main Content</h1></MainContent>
            <MainFooter></MainFooter>
          </Layout>
        </div>
      </Layout>
      <style>{`
        .app-layout {
          flex-direction: column !important;
        }

        .body {
          display: flex;
        }

        .layout-row {
          width: 100%;
        }

        .main-sider {
          height: 100vh;
        }

        @media (max-width: 576px) {
          .app-layout {
            display: flex;
          }

          .main-sider {
            height: 100%;
          }

          .body {
            flex-direction: column !important;
          }
        }
      `}</style>
    </>
  )
}

export default AppLayout
