import React, { ReactNode } from 'react'
import { Layout } from 'antd'

const { Footer } = Layout

interface Props {
  children: ReactNode
}

const MainFooter = (props: Props) => {
  return (
    <Footer style={{ textAlign: 'center' }}>
      {props.children}
    </Footer>
  )
}

MainFooter.defaultProps = {
  children: 'Ant Design ©2019 Created by HiepBC'
}

export default MainFooter
