export interface CurrentUser {
  avatar?: string;
  name?: string;
}
